package com.javaxpert.labs.java8.converter.ui;

import com.javaxpert.labs.java8.converter.tools.ZoneIdChecker;

import javax.swing.*;
import java.awt.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.stream.Collectors;

public class TimeConverterUI extends JFrame {
    private JButton computeButton;
    private JComboBox<String> zonesIdCb;
    private JTextField inputTf;
    private JTextField  outputTf;
    private DefaultComboBoxModel comboModel;


    public TimeConverterUI(){
        super("TimeConverter");
        initUI();
        initEvents();
    }

    // dispose les éléments graphiques à l'écran
    private void initUI(){
        JPanel main_panel = (JPanel)getContentPane();
        main_panel.setLayout(new FlowLayout());
        main_panel.add(inputTf=new JTextField());
        inputTf.setText(LocalDateTime.now().toString());
        zonesIdCb = new JComboBox<>();

        // une facon concise mais cracra de definir le modele de donnees de la combo
        // cf constructeur comboboxmodel
        zonesIdCb.setModel(comboModel = new DefaultComboBoxModel<>(
                ZoneId.getAvailableZoneIds().stream().filter(ZoneIdChecker.alternateAvailableZoneId).map((z) -> z.toString()).toArray()));

        main_panel.add(zonesIdCb);
        main_panel.add(computeButton = new JButton("convertir"));
        main_panel.add(outputTf=new JTextField());
        outputTf.setText("Date & heure converties dans le fuseau cible");

        // calcule la taille ideale et redimensionne
        pack();
        setVisible(true);

    }

    // gere les evenements
    private void initEvents(){
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        computeButton.addActionListener((evt) -> {
            inputTf.setText(LocalDateTime.now().toString());
            outputTf.setText(ZonedDateTime.ofInstant(Instant.now(),ZoneId.of((String)zonesIdCb.getSelectedItem())).toString());
        });

    }

    public static void main(String[] args)
    {
        new TimeConverterUI();

    }
}
