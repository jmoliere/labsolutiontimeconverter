package com.javaxpert.labs.java8.converter.tools;

import io.vavr.control.Try;

import java.time.ZoneId;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * This class contains a couple of util methods related to timezone handling
 * A couple of tilmezones as returned by old jdk 8 versions were plain wrong so filter them
 */
public class ZoneIdChecker {

    // check if we can instantiate a zone id from the given string
    public final static Predicate<String> availableZoneId = (zone) ->
    {
        try {
            ZoneId.of(zone);
            return true;
        } catch (Exception e) {
            return false;
        }
    };

    // same predicate with javaslang / vavr functional structure Try
    // shorter and more functional oriented
    public static final Predicate<String> alternateAvailableZoneId =
            (String zone) -> (Try.of(() -> ZoneId.of(zone)).isSuccess());

    /**
     * General service used to filter from any strings collection
     * the only available zones.
     *
     * @param inputList
     * @return
     */
    public List<String> extractAvailableZonesFrom(List<String> inputList) {
        return inputList.stream().
                filter(ZoneIdChecker.alternateAvailableZoneId)
                .collect(Collectors.toList());
    }
}
